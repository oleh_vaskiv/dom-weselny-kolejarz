<?php

namespace Ls\NewsBundle\Controller;

use Doctrine\DBAL\Types\Type;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * News controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all News entities.
     *
     */
    public function sectionAction() {
        $em = $this->getDoctrine()->getManager();

        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_news')->getValue();
        $allow = 1;

        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsNewsBundle:News', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        return $this->render('LsNewsBundle:Front:section.html.twig', array(
            'entities' => $entities,
            'allow' => $allow
        ));
    }
    
    public function ajaxMoreAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $page = $request->request->get('page');
        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_news')->getValue();
        
        $start = $page * $limit;
        $allow = 1;
        
        $today = new \DateTime();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsNewsBundle:News', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->orderBy('a.published_at', 'DESC')
            ->setParameter('today', $today, Type::DATETIME)
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        $response = array(
            'allow' => $allow,
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsNewsBundle:Front:list.html.twig', array(
                'entities' => $entities,
            ))->getContent())
        );

        return new JsonResponse($response);
    }
}
