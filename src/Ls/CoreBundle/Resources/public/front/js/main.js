$(document).on('click', '.mobile-menu-btn', function () {
    if (!$('.menu-mobile').is(':visible')) {
        $('.menu-mobile').fadeIn(250);
    }
});

$(document).on('click', '.hide-menu', function () {
    if ($('.menu-mobile').is(':visible')) {
        $('.menu-mobile').fadeOut(250);
    }
});


$('.gallery-container .item').lightGallery({
    thumbnail: true
}); 

$('.news-container .item .news-gallery-container').lightGallery({
    thumbnail: true
});

//send contact form
$( "#form_contact" ).submit(function( event ) {
    event.preventDefault();
    sendContact($(this).attr('action'), $(this).serialize())
});

//animate move on click menu
$('.nav').on('click', 'a', function (event) {
    event.preventDefault();
    var link = $(this).attr('href');
    
    if (checkLink(link)) {
        $('.menu-mobile').fadeOut(250);
        animateMenu(link);
    } else {
        if (link.length > 1) {
            window.location.replace(link);
        }
    }
});

$('footer').on('click', '.footer-nav', function (event) {
    event.preventDefault();
    var link = $(this).attr('href');
    
    if (checkLink(link)) {
        animateMenu(link);
    } else {
        if (link.length > 1) {
            window.location.replace(link);
        }
    }
});

$(document).on('click', '.navigate-mouse', function (event) {
    event.preventDefault();
    var link = $(this).attr('href');
    
    if (checkLink(link)) {
        animateMenu(link);
    } else {
        if (link.length > 1) {
            window.location.replace(link);
        }
    }
});

function checkLink(val) {
    var re = /#(.*)/;
    if (re.test(val) && val.length > 1) {
        return true;
    }

    return false;
}

function animateMenu(id) {
    var body = $("html, body");
    var screenWidth = window.innerWidth;
    
    var top = $(id).position().top;
    
    if (id === '#o-nas') {
        if (screenWidth >= 1200) {
            top = $(id).position().top-170;
        } else {
            top = $(id).position().top+5;
        }
    }

    body.stop(true, true).animate({scrollTop: top-100}, 1000, 'swing');
}

//sliders
$('.bxslider').bxSlider({
    auto: true,
    speed: 1000,
    controls: false,
    pager: false,
    mode: 'fade',
    onSliderResize: reloadBX,
    onSliderLoad: function () {
        $(".bxslider").css("visibility", "visible");
    }
});

slider=$('.partners-img').bxSlider({
    auto: true,
    speed: 2000,
    minSlides: 1,
    maxSlides: 5,
    slideWidth: 200,
    slideMargin: 45,
    controls: false,
    onSliderResize: reloadBX,
    onSliderLoad: checkBX,
});

/*
Resize Callback 
*/ // Stores the current slide index.
function reloadBX(idx) {
  localStorage.cache = idx;
  // Reloads slider, 
  ///goes back to the slide it was on before resize,
  ///removes the stored index.
  function inner(idx) {
    setTimeout(slider.reloadSlider, 0);
    var current = parseInt(idx, 10);
    slider.goToSlide(current);
    slider.startAuto();
    localStorage.removeItem("cache");
  }
}

/*
Load Callback
*/ // If the slider height is collapsed, 
 /////invoke a repaint and stay on current index.
function checkBX(idx) {
  var vp = $('.bx-viewport').height();
  while (vp <= 0) {
    slider.redrawSlider();
  }
  slider.goToSlide(idx);
}

//load news
var news_loading_page = 1;
function loadNews(allow, url) {
    var news_loading_allow = allow;

    if (news_loading_allow == true) {
        $('.news .list-ajax-loader').slideToggle('fast');
        $.ajax({
            type: 'post',
            url: url,
            data: {
                page: news_loading_page
            },
            success: function (response) {
                news_loading_page++;
                news_loading_allow = response.allow;
                if (news_loading_allow === 0) {
                    $('.news .more').hide();
                }
                $('.news .news-container').append(response.html);
                $('.news .news-container').addClass('active');
                $('.news .list-ajax-loader').hide();
                
                $('.news-container .item .news-gallery-container').lightGallery({
                    thumbnail: true
                });
            }
        });
    }
}
 
//parallax
skrollr.init({
    smoothScrolling: false,
    mobileDeceleration: 0.004,
    mobileCheck: function () {
        //hack - forces mobile version to be off
        return false;
    }
});
$(window).on('resize', function () {
    skrollr.init({
        smoothScrolling: false,
        mobileDeceleration: 0.004,
        mobileCheck: function () {
            //hack - forces mobile version to be off
            return false;
        }
    });
});


/**
 * Update navigation links on scroll
 */
function updateNavigationLinks() {
	var scrollPos = $(window).scrollTop();

	$('.navbar-nav a').each(function () {
		var currLink = $(this);

        var link = currLink.attr('href');
        if (checkLink(link)) {	
            var refElement = $(link);

    		if (refElement.offset().top-100 <= scrollPos && refElement.offset().top-100 + refElement.height() > scrollPos) {
    			
    			currLink.addClass('current');
    		}
    		else {
    			currLink.removeClass('current');
    		}
        }
	});
}

$(window).on('scroll', updateNavigationLinks);

//add shadow to menu on scroll
$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if (scroll > 1) {
       $('.fixed-header').addClass('shadow');
       
    } else {
        $('.fixed-header').removeClass('shadow');
        
   } 
});

