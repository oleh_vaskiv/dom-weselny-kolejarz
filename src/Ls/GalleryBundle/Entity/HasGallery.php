<?php

namespace Ls\GalleryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HasGallery
 * @ORM\MappedSuperclass
 */
class HasGallery
{
    /**
     * @ORM\ManyToOne(
     *     targetEntity="Ls\GalleryBundle\Entity\Gallery",
     * )
     * @ORM\JoinColumn(
     *     name="gallery_id",
     *     referencedColumnName="id",
     * )
     * @var \Ls\GalleryBundle\Entity\Gallery
     *
     */
    private $gallery;

    /**
     * Set gallery
     *
     * @param \Ls\GalleryBundle\Entity\Gallery $gallery
     * @return HasGallery
     */
    public function setGallery(Gallery $gallery = null)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \Ls\GalleryBundle\Entity\Gallery
     */
    public function getGallery()
    {
        return $this->gallery;
    }
}