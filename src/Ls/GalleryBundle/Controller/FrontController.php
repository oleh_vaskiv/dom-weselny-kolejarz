<?php

namespace Ls\GalleryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Gallery controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Gallery entities.
     *
     */
    public function sectionAction() {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a', 'p')
            ->from('LsGalleryBundle:Gallery', 'a')
            ->leftJoin('a.photos', 'p')
            ->where('a.on_list = 1')
            ->orderBy('a.id', 'asc')
            ->addOrderBy('p.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        return $this->render('LsGalleryBundle:Front:section.html.twig', array(
            'entities' => $entities,
        ));
    }
}
